package cep;

import java.io.RandomAccessFile;

public class Main {
 
    public static void main(String[] args) throws Exception {
         
        RandomAccessFile f = new RandomAccessFile("cep_ordenado.dat", "r");         
        busca(f, args[0]);
      
    }
    
    public static void busca(RandomAccessFile f, String cep) throws Exception {
    	long tamanhoArquivo = f.length();
    	long qtdRegistros = tamanhoArquivo/300;
    	long log = (long) (Math.log10(qtdRegistros) / Math.log10(2.));
    	long count = 0;
    	
    	
    	long inicio = 0;
    	long meio = qtdRegistros/2;
    	long fim = qtdRegistros;
    	
    	while (count <= log){
    		f.seek(meio*300);           
    		
            Endereco e = new Endereco();
            e.leEndereco(f);
            
            if(e.getCep().equals(cep)){
            	printar(e);
            	break;
            } else if(Integer.parseInt(e.getCep()) > Integer.parseInt(cep) ){
            	fim = meio -1;
            	meio = (inicio + fim) /2;            	
            } else if(Integer.parseInt(e.getCep()) < Integer.parseInt(cep) ){
            	inicio = meio + 1;
            	meio = (inicio + fim) /2;
            }   
    	}
    	f.close();
   
    }
    
    public static void printar(Endereco e){
         System.out.println(e.getLogradouro());
         System.out.println(e.getBairro());
         System.out.println(e.getCidade());
         System.out.println(e.getEstado());
         System.out.println(e.getSigla());
         System.out.println(e.getCep());
    }
 
}